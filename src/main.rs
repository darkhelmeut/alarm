mod list_of_radios;
mod play_radio;

use list_of_radios::{list_of_radios, radio_url};
use play_radio::play_radio;

use gtk4::{glib, prelude::*};

fn main() -> glib::ExitCode {
    let application = gtk4::Application::builder()
        .application_id("theDarky.alarm.librem5.gtk4")
        .build();
    application.connect_activate(build_ui);
    application.run()
}

fn build_ui(application: &gtk4::Application) {
    let ui_src = include_str!("alarm.ui");
    let builder = gtk4::Builder::from_string(ui_src);

    let window = builder
        .object::<gtk4::ApplicationWindow>("window")
        .expect("Couldn't get window");
    window.set_application(Some(application));

    let radios = list_of_radios();


    let play_button = builder
        .object::<gtk4::Button>("playRadioButton")
        .expect("Couldn't get button");

    play_button.connect_clicked(move |_| {
        println!("play_button");
    });


    let stop_button = builder
        .object::<gtk4::Button>("stopRadioButton")
        .expect("Couldn't get button");

    stop_button.connect_clicked(move |_| {
        println!("stop_button");
    });


    window.present();
}