extern crate yaml_rust;
use std::fs;
use yaml_rust::{YamlLoader, Yaml};

use lazy_static::lazy_static;

lazy_static! {
    static ref YAML: Vec<Yaml> = load_radios();
}

fn load_radios() -> Vec<Yaml> {
    let yaml_file = fs::read_to_string("src/radios.yml").unwrap();

    YamlLoader::load_from_str(&yaml_file).unwrap()
}

pub fn radio_url(name: &String) -> String {

    let doc = &YAML[0];
    
    let url = doc[&name[..]].as_str().unwrap();

    String::from(url)
}

pub fn list_of_radios() -> Vec<String> {
    let mut vec = Vec::new();

    for k in YAML.iter() {
        let hash =k.as_hash().unwrap();
        for (k,_) in hash {
            let key = k.as_str().unwrap();
            vec.push(String::from(key));
        }
      
    }
    return vec;
}