use std::error::Error;
use std::io::Read;
use std::result::Result;

use stream_download::http::HttpStream;
use stream_download::http::reqwest::Client;
use stream_download::source::SourceStream;
use stream_download::storage::temp::TempStorageProvider;
use stream_download::{Settings, StreamDownload};
use std::io::BufReader;

#[tokio::main]
pub async fn play_radio(url: &String)-> Result<(), Box<dyn Error>> {
    let stream =
        HttpStream::<Client>::create(url.parse()?).await?;
    let content_length = stream.content_length();
    let is_infinite = content_length.is_none();
    println!("Infinite stream = {is_infinite}");

    let mut reader =
        StreamDownload::from_stream(stream, TempStorageProvider::default(), Settings::default())
            .await?;

    let mut buf = [0; 256];
    reader.read_exact(&mut buf)?;


    let (_stream, handle) = rodio::OutputStream::try_default().unwrap();
    let sink: rodio::Sink = rodio::Sink::try_new(&handle).unwrap();

    sink.append(rodio::Decoder::new(BufReader::new(reader)).unwrap());

    sink.sleep_until_end();

    Ok(())
}